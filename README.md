**Aplicación de Test para 2BRAIN**

Esta aplicación solicita autenticación con Google para acceder a la lista de Usuarios([API RandomUser](https://randomuser.me/)). Los datos obtenidos se almacenan en caché 60 segundos, además se agregó un 10% de probabilidad de que la llamada al servicio del backend responda error 500 guardando un log en /logs, el frontend reiniciará la llamada hasta que sea correcta mostrando en consola cada vez que falle.

*Aplicación realizada con React JS como front-end y Node JS /Express en el back-end.*

---

## Como ejecutar el proyecto en un ambiente local

Para ejecutar el proyecto debes tener instalado REDIS en tu equipo, puedes usar este articulo para instalar Redis en [Windows](https://redislabs.com/ebook/appendix-a/a-3-installing-on-windows/a-3-2-installing-redis-on-window/).

1. Una vez descargado el proyecto, ejecuta en la carpera raiz **npm install**.
2. Dentro de la carpeta **/client** se encuentra el FRONT-END, por lo que tambien deberás ejecutar **npm install** dentro de esa carpeta.
3. Para levantar la aplicación debes tener el server de REDIS funcionando.
4. Volver a la carpeta raiz y ejecutar **npm run dev**