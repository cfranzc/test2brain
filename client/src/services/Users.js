import axios from 'axios';

const fetch_retry = async (url, token) => {
    try {
        return await axios.get(url, {
            headers:
                { authorization: `Bearer ${token}` }
        }).then(res => res.data)
    } catch(err) {
        console.log("Error de servicio ")
        return await fetch_retry(url, token);
    }
};

function getUsers(token) {

    let users = fetch_retry('/api/user',token)
    return users
}



export default { getUsers }