import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import CachedIcon from '@material-ui/icons/Cached';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';

import Lista from '../home/Lista'
import UsersWS from '../../services/Users'

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Desarrollo usando Material UI
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'left',
  },
  title: {
    marginBottom: 8,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  header: {
    flexGrow: 1,
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
}));

const Home = props => {

  const classes = useStyles();

  const [data, setData] = React.useState(null)
  const [loading, setLoading] = React.useState(true)
  const [postLoading, setPostLoading] = React.useState(false)
  const [open, setOpen] = React.useState(false);
  const [textSnack,setTextSnack] = React.useState("Actualizando datos")

  const getData = (precharge) => {
    if (precharge) {
      setLoading(true)
    }
    else {
      if(open) handleClose()
      handleClick()
      setPostLoading(true)
    }
    if (props.user) {
      props.user.getIdToken().then(async token => {
        let data = await UsersWS.getUsers(token)
        setData(data)
        setLoading(false)
        setPostLoading(false)
        setTextSnack("Datos actualizados")
      })
    }
  }
  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  React.useEffect(() => {
    getData(true)
  }, [])
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={3} md={3} className={classes.image} />
      <Grid item xs={12} sm={9} md={9} component={Paper} elevation={6} square>
        <div >
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" className={classes.header}>
                Test
          </Typography>
              <Button color="inherit" onClick={props.signOut}>Salir</Button>
            </Toolbar>
          </AppBar>
        </div>
        <div className={classes.paper}>
          <Typography component="h1" variant="h4" align="center" className={classes.title}>
            Usuarios
          </Typography>
          {
            loading ? <CircularProgress style={{ alignSelf: "center" }} /> : <Lista {...data} />
          }
          <Fab aria-label="Edit" className={classes.fab} disabled={postLoading} color="secondary" onClick={() => getData(false)}>
            <CachedIcon />
          </Fab>
          <Box mt={8}>
            <Copyright />
          </Box>
        </div>
      </Grid>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message={textSnack}
        action={
          <React.Fragment>
            <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
    </Grid>
  );
}

export default Home