import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const Detail = props => {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Ver
      </Button>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">{props.name.title + " " + props.name.first}</DialogTitle>
        <DialogContent>
          <Avatar alt="Remy Sharp" src={props.picture.thumbnail} />
          <Typography variant="caption" display="block" gutterBottom style={{marginTop:10}}> 
            Nombre
          </Typography>
          <Typography variant="body1" gutterBottom>
            {props.name.first + " " + props.name.last}
          </Typography>

          <Typography variant="caption" display="block" gutterBottom>
            Email
            </Typography>
          <Typography variant="body1" gutterBottom>
            {props.email}
          </Typography>

          <Typography variant="caption" display="block" gutterBottom>
            Género
            </Typography>
          <Typography variant="body1" gutterBottom>
            {props.gender}
          </Typography>

          <Typography variant="caption" display="block" gutterBottom>
            Localización
            </Typography>
          <Typography variant="body1" gutterBottom>
            {props.location.street.name + " " + props.location.street.number + ", " + props.location.city + ", " + props.location.country}
          </Typography>

          <Typography variant="caption" display="block" gutterBottom>
            Fono
            </Typography>
          <Typography variant="body1" gutterBottom>
            {props.phone}
          </Typography>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cerrar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default Detail