import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';

import Detail from './Detail'

const useStyles = makeStyles({
  table: {
    minWidth: 100,
  },
});

const Lista = data => {

  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <Hidden smDown><TableCell align="right">Email</TableCell></Hidden>
            <Hidden smDown><TableCell align="right">Genero</TableCell></Hidden>
            <TableCell align="right">Ciudad</TableCell>
            <TableCell align="right"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            data && (
              Object.keys(data).map((keyName, i) => (
                <TableRow key={i}>
                  <TableCell component="th" scope="row">
                    {data[keyName].name.first}
                  </TableCell>
                  <Hidden smDown><TableCell align="right">{data[keyName].email}</TableCell></Hidden>
                  <Hidden smDown><TableCell align="right">{data[keyName].gender}</TableCell></Hidden>
                  <TableCell align="right">{data[keyName].location.city}</TableCell>
                  <TableCell align="right">{<Detail {...data[keyName]} />}</TableCell>
                </TableRow>
              ))
            )
          }
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default Lista