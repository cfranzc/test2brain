import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import CssBaseline from '@material-ui/core/CssBaseline'
import withFirebaseAuth from 'react-with-firebase-auth'
import * as firebase from 'firebase/app';
import 'firebase/auth'

import firebaseConfig from './components/firebase/firebase'
import Home from './components/activities/Home'
import Login from './components/activities/Login'
import { ProtectedRoute } from './utils/protected.route'

const firebaseApp = firebase.initializeApp(firebaseConfig);

class App extends React.Component {

  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Router>
          <Switch>
            <Route exact path="/">
              <Login {...this.props} />
            </Route>
            <ProtectedRoute {...this.props} extact path="/home" component={Home}>
            </ProtectedRoute>
            <Route>
              <Login {...this.props} />
            </Route>
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}

const firebaseAppAuth = firebaseApp.auth();

const providers = {
  googleProvider: new firebase.auth.GoogleAuthProvider(),
};

export default withFirebaseAuth({
  providers,
  firebaseAppAuth,
})(App);
