const redis = require("redis");

const portRedis = require('../utils/RedisConfig')

const redis_client = redis.createClient(portRedis.port_redis)

//Función para validar si la data se encuentra disponible en el cache
checkCacheData = (req, res, next) => {
    console.log("--revisando en cache")
    redis_client.get("data", (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).send(err);
        }
        if (data != null) {
            console.log("si esta en cache")
            res.send(data);
        } else {
            next();
        }
    });
};

module.exports = { checkCacheData }