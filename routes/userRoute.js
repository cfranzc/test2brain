const fs = require('fs');
const logger_error = fs.createWriteStream('logs/log_error.txt', { flags: 'a' })

const users = require('../controllers/Users')
const { checkCacheData } = require('../middlewares/Redis')

module.exports = (app) => {
    app.get(`/api/user`, checkCacheData, (req, res) => {
        //Agregando 10% de probabilidad de que "no funcione" el servicio
        if (Math.random() < 0.2) {
            logger_error.write(String(new Date()) + ` Error - No se pudo ejecutar por error "Random" \n`)
            res.status(500).send("Error finjido");
        } else {
            users.getUsers()
                .then((data) => {
                    return res.status(200).send(data);
                })
                .catch(err => {
                    logger_error.write(String(new Date()) + ` Error - No se pudo ejecutar por error con API Random \n`)
                    return res.status(500).send("Error finjido")
                }
                )
        }

    });

    app.get(`/api/user/:id`, async (req, res) => {
        const { id } = req.params
        let client = { id: id, name: "Carlos" };
        return res.status(200).send(client);
    });
}