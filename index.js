const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const admin = require('firebase-admin')

require('dotenv').config()

const app = express()

const serviceAccount = require('./config/test2brain-firebase-adminsdk-y55u5-5728f2dbf6.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://test2brain.firebaseio.com"
});

app.use(cors())

app.use(bodyParser.json())

//Función para validar el token de Firebase
function checkAuth(req, res, next) {
    if (req.headers.authorization &&
        req.headers.authorization.split(' ')[0] === 'Bearer') {
        let authToken = req.headers.authorization.split(' ')[1];
        admin.auth().verifyIdToken(authToken)
            .then(() => {
                next()
            }).catch(() => {
                res.status(403).send('Unauthorized')
            });
    } else {
        res.status(403).send('Unauthorized')
    }
}

//Agregar la función de validación de token a todas las rutas de API
app.use('/api/', checkAuth)

//IMPORT ROUTES
require('./routes/userRoute')(app);

if (process.env.NODE_ENV === 'production') {
    //Utilizar la build React JS
    app.use('/', express.static(`${__dirname}/client/build`))

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    })
}

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`APP corriendo en el puerto ${PORT}`)
})