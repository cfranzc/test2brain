const axios = require('axios')
const redis = require("redis");

const portRedis = require('../utils/RedisConfig')
const redis_client = redis.createClient(portRedis.port_redis)

getUsers = async () => {

    console.log("no esta en cache")
    const userData = await axios.get(
        `https://randomuser.me/api/?results=10`
    );
    //Guardar los datos en redis con 60 segundos
    redis_client.setex("data", 60, JSON.stringify(userData.data.results));

    return userData.data.results
}

getUser = (id) => {

}
module.exports = { getUsers, getUser }